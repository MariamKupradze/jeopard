
---

## აპლიკაცია Jeopardy და წესები


 **ზოგადი ინფორმაცია**
 
 ეს არის აპლიკაცია - Jeopardy . ე.წ "სვაიაკი",წააგავს ინტელექტუალურ თამაშს "რა?სად?როდის?"
 აპლიკაციას იყენებს უშუალოდ წამყვანი.
 
 . წესები შემდეგნაირია:
 1.მოთამაშეთა რაოდენობა არ არის შეზღუდული(მთავარია იყოს კენტი. )
 2.მოთამაშეები იყოფიან ორ გუნდად
 3.ერთი უნდა იყოს წამყვანი,ვინც უშუალოდ აპლიკაციას გამოიყენებს
 4.ირჩევს სასურველ პაკეტს
 5.კითხვის კითხვისას,მოთამაშეებმა ვერსიის ქონის შემთხვევაში  ტაშით უნდა მიანიშნონ წამყვანს.
 6.სწორი პასუხის შემთხვევაში  წამყვანი ,უმატებს გუნდს კითხვაზე მითითებულ ქულას.
 7.არასწორი პასუხის შემთხვევაში წამყვანი აგრძელებს კითხვას  მეორე გუნდთან.
 8.კითხვის დასრულებისას მოთამაშეებს აქვთ 3 წამი ვერსიის სათქმელად.


---

## ჯერ კიდევ მიმდინარეობს აპლიკაციის დამუშავება



1. პაკეტების გვერდზე პასუხების ღილაკს აკლია toogle ფუნქცია
2. გამოყენებულია ერთი და იგივე ტექსტები მაგალითისთვის
3.არ აქვს log out 


## ვერსია
1.0.2

## ავტორი
მარიამ კუპრაძე






---